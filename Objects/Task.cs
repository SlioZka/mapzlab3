﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3.Objects
{
    public class Task : IObject
    {
        public string title { get; set; }
        public string date { get; set; }
        public string description { get; set; }
        public Task(string _title, string _date, string _description)
        {
            title = _title;
            date = _date;
            description = _description;
        }
        public IObject Clone()
        {
            return new Task(this.title, this.date, this.description);
        }
        public void ShowInfo()
        {
            Console.WriteLine("Task: {0}\nWhen: {1}\nDescription: {2}\n", title, date, description);
        }
    }
}
