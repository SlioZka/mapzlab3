﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3.Objects
{
    public class Reminder :IObject
    {
        public string title { get; set; }
        public string date { get; set; }
        public Reminder(string _title, string _date)
        {
            title = _title;
            date = _date;
        }
        public IObject Clone()
        {
            return new Reminder(this.title, this.date);
        }
        public void ShowInfo()
        {
            Console.WriteLine("{0}\nWhen: {1}\n", title, date);
        }
    }
}
