﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3
{
    class Date
    {
        private static Date instance;
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        private Date() { }
        private Date(int _day, int _month, int _year)
        {
            day = _day;
            month = _month;
            year = _year;
        }
        public static Date GetInstance(int _day, int _month, int _year)
        {
            if (instance == null)
            {
                instance = new Date(_day, _month, _year);
            }
            return instance;
        }
        
    }
}
