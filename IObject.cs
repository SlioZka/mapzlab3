﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3
{
    public interface IObject
    {
        IObject Clone();
        void ShowInfo();
    }
}
