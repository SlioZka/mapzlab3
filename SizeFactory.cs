﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3
{
    abstract class Tasks { }
    abstract class Info { }
    abstract class SizeFactory
    {
        public abstract Tasks CreateTasks();
        public abstract Info CreateInfo();
    }
    class BigTasks : Tasks {
    public BigTasks()
        {
            Console.WriteLine("There are 8 Tasks, at least.");
        }
    }
    class BigInfo : Info { 
    public BigInfo()
        {
            Console.WriteLine("There are all info about you.");
        }
    }
    class BigFactory : SizeFactory
    {
        public override Tasks CreateTasks()
        {
            return new BigTasks();
        }
        public override Info CreateInfo()
        {
            return new BigInfo();
        }
    }
    class SmallTasks : Tasks {
    public SmallTasks()
        {
            Console.WriteLine("There are 4 tasks.");
        }
    }
    class SmallInfo : Info {
    public SmallInfo()
        {
            Console.WriteLine("There are nickname and your avatar");
        }
    }
    class SmallFactory : SizeFactory
    {
        public override Tasks CreateTasks()
        {
            return new SmallTasks();
        }
        public override Info CreateInfo()
        {
            return new SmallInfo();
        }
    }
}
