﻿using MAPZLab3.Objects;
using System;

namespace MAPZLab3
{
    class Program
    {
        static void Main(string[] args)
        {
            //fasad

            //Showing the Singleton
            //Events Eve1 = Events.GetInstance();
            //Eve1.ShowInfo();
            //Events Eve2 = Events.GetInstance();
            //Eve2.Data[2] = "Call a police";
            //Eve1.ShowInfo();
            //Eve2.ShowInfo();

            //Showing the Abstract Factory
            //Console.WriteLine("Do u have (B)ig or (S)mall screen???");
            //char input = Console.ReadKey().KeyChar;
            //SizeFactory factory;
            //switch (input)
            //{
            //    case 'B':
            //        factory = new BigFactory();
            //        break;

            //    case 'S':
            //        factory = new SmallFactory();
            //        break;

            //    default:
            //        throw new NotImplementedException();

            //}
            //Console.WriteLine();
            //var task = factory.CreateTasks();
            //var info = factory.CreateInfo();

            //Show Prototype
            //IObject task1 = new Task("Doctor visit", "24.05.21", "Call mr.Devi for more info\nDevi phone: ABC-CBA");
            //Task task2 = new Task("Call the police", "22.05.20021", "POlice phone 102");
            //IObject clonedTask = task1.Clone();
            //Task task3 = (Task)task2.Clone();
            //task3.title = "buy soda";
            //task1.ShowInfo();
            //task2.ShowInfo();
            //task3.ShowInfo();
            //clonedTask.ShowInfo();

            //IObject rem1 = new Reminder("Buy milk", "11.05.21");
            //IObject clonedRem = rem1.Clone();
            //rem1.ShowInfo();
            //clonedRem.ShowInfo();
        }
    }
}
