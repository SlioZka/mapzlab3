﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3
{
    class Events
    {
        private static Events instance = null;
        private static readonly object padlock = new object();
        public string[] Data { get; set; }
        Events()
        {
            //Data base
            Data = new string[4];
            Data[0] = "Walk with dog";
            Data[1] = "Call a doctor";
            Data[2] = "Buy milk, eggs, meat and salt";
            Data[3] = "Send a CV";
        }
        public static Events GetInstance()
        {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Events();
                    }
                    return instance;
                }
        }
        public void ShowInfo()
        {
            foreach(string i in Data)
                Console.WriteLine(i);
            Console.WriteLine();
        }
    }
}
