﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MAPZLab3
{
    class Calendar
    {
        public Date Day { get; set; }
        public void CheckDay(Date _date)
        {
            Day = Date.GetInstance(_date.day, _date.month, _date.year);
        }
        public void ShowInfo()
        {
            Console.WriteLine("Today is {0}/{1}/{2}", Day.day, Day.month, Day.year);
        }
    }
}
